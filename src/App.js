import React from 'react';
import AddAnimalForm from './AddAnimalForm';
import AnimalList from './AnimalList';

const App = () => {
  return (
    <div>
      <h1>Добавить животного</h1>
      <AddAnimalForm />
      <AnimalList />
    </div>
  );
};

export default App;
