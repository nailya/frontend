import axios from 'axios';

const API_URL = 'http://localhost:9001/api/animals';

export const addAnimal = async (animalData) => {
    try {
        const response = await axios.post(API_URL, animalData);
        return response.data;
    } catch (error) {
        console.error('Error adding animal:', error);
    }
};

export const getAllAnimals = async () => {
    try {
        const response = await axios.get(API_URL);
        return response.data;
    } catch (error) {
        console.error('Error fetching animals:', error);
    }
};
