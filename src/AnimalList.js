import React, { useState, useEffect } from 'react';
import { getAllAnimals } from './api';

const AnimalList = () => {
    const [animals, setAnimals] = useState([]);

    const fetchAnimals = async () => {
         const animalsData = await getAllAnimals();
         setAnimals(animalsData);
    };

    useEffect(() => {
        fetchAnimals();
    }, []);

    return (
        <div>
            <h2>Список животных</h2>
            <ul>
                {animals && animals.map((animal) => (
                    <li key={animal.id}>{animal.name} - {animal.species}</li>
                ))}
            </ul>
        </div>
    );
};

export default AnimalList;
