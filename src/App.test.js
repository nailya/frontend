import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import axios from 'axios';
import AddAnimalForm from './AddAnimalForm';
import AnimalList from './AnimalList';

jest.mock('axios');

describe('AddAnimalForm component', () => {
  test('Submitting the form calls addAnimal function with correct data', async () => {
    axios.post.mockResolvedValueOnce({ data: { id: 1, name: 'Test Animal', species: 'Test Species' } });

    const { getByPlaceholderText, getByText } = render(<AddAnimalForm />);

    const nameInput = getByPlaceholderText('Имя');
    const speciesInput = getByPlaceholderText('Вид');
    const addButton = getByText('Добавить животное');

    fireEvent.change(nameInput, { target: { value: 'Test Animal' } });
    fireEvent.change(speciesInput, { target: { value: 'Test Species' } });
    fireEvent.click(addButton);

    await waitFor(() => {
      expect(axios.post).toHaveBeenCalledWith('http://localhost:9001/api/animals', { name: 'Test Animal', species: 'Test Species' });
      expect(nameInput.value).toBe('');
      expect(speciesInput.value).toBe('');
    });
  });
});

describe('AnimalList component', () => {
  test('Renders the list of animals fetched from the API', async () => {
    const mockAnimals = [
      { id: 1, name: 'Animal 1', species: 'Species 1' },
      { id: 2, name: 'Animal 2', species: 'Species 2' },
    ];

    axios.get.mockResolvedValueOnce({ data: mockAnimals });

    const { getByText } = render(<AnimalList />);

    await waitFor(() => {
      mockAnimals.forEach(animal => {
        expect(getByText(`${animal.name} - ${animal.species}`)).toBeInTheDocument();
      });
    });
  });
});
