import React, { useState } from 'react';
import { addAnimal } from './api';

const AddAnimalForm = () => {
  const [name, setName] = useState('');
  const [species, setSpecies] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!name || !species) return;
    await addAnimal({ name, species });
    setName('');
    setSpecies('');
    window.location.reload();
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="Имя"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <input
        type="text"
        placeholder="Вид"
        value={species}
        onChange={(e) => setSpecies(e.target.value)}
      />
      <button type="submit">Добавить животное</button>
    </form>
  );
};

export default AddAnimalForm;
